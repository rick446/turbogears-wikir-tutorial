.. TurboGears Wikier Tutorial documentation master file, created by
   sphinx-quickstart on Fri Oct 12 20:19:44 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to TurboGears Wikier Tutorial's documentation!
======================================================

TurboGears2 Wikier application is inspired by the
`TurboGears2 20 Minutes Wiki <http://www.turbogears.org/book/part1/wiki20.html>`_ Tutorial.

While the 20 Minutes Wiki is a great way to learn writing custom TurboGears2 applications
the Wikier tutorial tries to provide more focus on rapid prototyping with the ambitious target of
making it possible to create a full featured Wiki in nearly 5 minutes.

It will showcase some basic concepts of the TurboGears2 Web Framework and how to best use rapid prototyping tools
available in the standard extension modules.

Contents:

.. toctree::
   :maxdepth: 2

   quickstart
   models
   controllers
   admin
   caching

Documentation Links
~~~~~~~~~~~~~~~~~~~~~

 * **TurboGears2**: http://www.turbogears.org/2.2/docs/
 * **Genshi** XML Templates: http://genshi.edgewall.org/wiki/Documentation/xml-templates.html
 * **SQLAlchemy** : http://www.sqlalchemy.org/
 * **ToscaWidgets2** : http://tw2core.readthedocs.org/en/latest/
 * **tgext.datahelpers** : http://pypi.python.org/pypi/tgext.datahelpers
 * **tgext.crud** : http://turbogears.org/2.2/docs/main/Extensions/Crud/index.html


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

